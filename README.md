# :no_entry: This repository is obsolete
This repo is now obsolete. The content has been moved to this repo https://github.com/cloudentity/orchis-jboss-sso-authenticator.git

https://syntegritynet.atlassian.net/wiki/spaces/OISIAM/pages/116266602/OrchIS+JBoss+declarative+security+support

##Introduction
In Java EE, the component containers are responsible for providing application security and 
Declarative security is one of the mechanism to support container security. OrchIS provides support 
for declarative security model in a container even when perimeter or centralized authentication was 
used to authenticate a user or entity. OrchIS provides a module to allow web applications to 
integrate with JEE declarative security support for users authenticated by OrchIS. Web applications 
can define roles based on high level groups or information from centralized session to further 
determine low level access into the application using the login context, authenticated subject
and the derived roles for the subject using Java EE specifications.

###Configure Generic Header Authentication Valve
Authentication valve is the main interceptor for protected resource security. This intercepts the
requests, ensure required headers are present and forwards to to underlying custom login module to 
validate the incoming credentials and map roles to associated subject entity.
Below entries must be made in jboss-web.xml(~web-app/web/src/main/webapp/WEB-INF/jboss-web.xml) of 
the web application to configure the valve and headers required for the valve.

```
<?xml version="1.0" encoding="UTF-8"?>
<jboss-web>
    <valve>
        <class-name>org.jboss.as.web.security.GenericHeaderAuthenticator</class-name>
        <param>
            <param-name>httpHeaderForSSOAuth</param-name>
            <param-value>iv-user</param-value>
        </param>
        <param>
            <param-name>sessionCookieForSSOAuth</param-name>
            <param-value>sfgToken</param-value>
        </param>
    </valve>
    <security-domain>ciam-orchis-auth</security-domain>
    <disable-audit>true</disable-audit>
</jboss-web>
```

#####Note:
Valve expects 2 headers in incoming requests for protected requests. If either one of this is not 
available and a protected resource is being fetched, then HTTP 500 would be returned. There is no 
fallback mechanism and hence 500 is being returned. The only fallback mechanism at this point is to 
redirect user to centralized login page rather than local authentication.

* iv-user â†’ this represents the identifier of authenticated user.
* sfgToken â†’ this represents the authenticated session token from orchis
* "security-domain" value must be same as the one declared in the security subsystem within 
standalone.xml of the servers to reference underlying login module implementation.

###Configure security subsystem & domain
Configure and enable security domain with name "ciam-orchis-auth" in standalone.xml for the
jboss server. 
_Note that this name should match the value given in security-domain in jboss-web.xml. 
If not, the war file would complain during start up._

```
<subsystem xmlns="urn:jboss:domain:security:1.2">
   <security-domains>
       <security-domain name="other" cache-type="default">
           <authentication>
               <login-module code="Remoting" flag="optional">
                   <module-option name="password-stacking" value="useFirstPass"/>
               </login-module>
               <login-module code="RealmDirect" flag="required">
                   <module-option name="password-stacking" value="useFirstPass"/>
               </login-module>
           </authentication>
       </security-domain>
       <security-domain name="jboss-web-policy" cache-type="default">
           <authorization>
               <policy-module code="Delegating" flag="required"/>
           </authorization>
       </security-domain>
       <security-domain name="jboss-ejb-policy" cache-type="default">
           <authorization>
               <policy-module code="Delegating" flag="required"/>
           </authorization>
       </security-domain>
       <security-domain name="ciam-orchis-auth" cache-type="default">
           <authentication>
               <login-module code="com.orchis.eai.jboss.jaas.authenticator.OrchISLoginModule" flag="required" module="ciam-auth"/>
           </authentication>
       </security-domain>
   </security-domains>
 </subsystem>
 ```
       
###Configure and add OrchIS login Module

####Create a new module directory within jboss server layout to host the orchis login module.
Layout structure is shown as below
Directory - jboss-eap-6.4/modules/ciam-auth/main
##### Files
* module.xml
* role-mapping.properties
* orchis-connection.properties
* orchis-jboss-sso-authenticator-1.1.jar

#### module.xml
This file has the jboss module configuration for orchis login module , dependencies and the 
path for resources required by the module.

```
<?xml version="1.0" encoding="UTF-8"?>
<module xmlns="urn:jboss:module:1.1" name="ciam-auth">
  <resources>
  <resource-root path="."/>
  <resource-root path="orchis-jboss-sso-authenticator-1.1.jar"/>
  </resources>
 <dependencies>
    <module name="javax.api"/>
    <module name="javax.servlet.api"/>
    <module name="org.jboss.logging"/>
    <module name="org.picketbox"/>
    <module name="org.jboss.as.web"/>
    <module name="org.slf4j"/>
    <module name="org.apache.httpcomponents"/>
  </dependencies>
</module>
```

#### orchis-connection.properties 
Properties to configure the connection to underlying CIAM provider.
```
url=<protocol>://<orchis-sla-hostname>:<port>/overlay/api/session
cookieName=sfgToken
fetchGroupsFromSession=true
connectTimeoutInMillis=10000
readTimeoutInMillis=10000
maxTotal=100
maxPerRoute=10
trustAllCertificates=false
trustSelfSignedCertificates=false
ignoreNotMatchingHostnameInCertificate=false
reapInterval=300000
idleTimeout=60
```

* url - location of orchis REST services to fetch groups based on token
* cookieName - Name of orchis token injected as cookie after authentication
* connectTimeoutInMillis - Time to wait for a connection to be established with OrchIS services
* readTimeoutInMillis - Time to wait for response for an established connection with OrchIS services
* fetchGroupsFromSession - flag to indicate whether to fetch user groups from session or not
* maxTotal - Maximum number of connections
* maxPerRoute - Maximum number of connections per route
* trustAllCertificates - Trust certificates regardless of validity
* trustSelfSignedCertificates - Trust self-signed certificates
* ignoreNotMatchingHostnameInCertificate - Ignore certificates where hostnames don't match
* reapInterval - Interval for when the thread to reap stale connections should run (in milliseconds)
* idleTimeout - Reap threads that have been idle longer than the specified timeout (in seconds)

#### role-mapping.properties
Properties to configure the application specific role mapping requirements.

```
defaultRole=some-dummy-role
roleFilePath=/opt/idam/some-location/src/main/resources/user-roles.properties
```

####  user-roles.properties
Contents of user-roles.properties is a json mapping of roles to groups. The keys are the locally defined rules and values are the groups from the centralized session server associated to the user.
```
{
"admin": ["LDAP_ACCESS_ADMIN", "some_random_group", "admin"],
"developer": ["LDAP_ACCESS_ADMIN", "some_random_group", "admin"],
"AuthorizedUser": ["APP_DEV_GROUP", "DEV_GROUP"]
}
```
defaultRole is the role that get associated to a session user regardless of whether user 
belongs to any group or not.

##TroubleShooting

1.`[org.apache.ws.security.WSSConfig] (MSC service thread 1-2) The provider BC could not be added: org.bouncycastle.jce.provider.BouncyCastleProvider 
from [Module "org.apache.ws.security:main" from local module loader @707f7052 (finder: local module finder @11028347 
(roots: /Users/rbabu/projects/softwares/jboss-eap-6.4/modules,/Users/rbabu/projects/softwares/jboss-eap-6.4/modules/system/layers/base))]: 
java.lang.ClassNotFoundException: org.bouncycastle.jce.provider.BouncyCastleProvider from [Module "org.apache.ws.security:main" from local module
 loader @707f7052 (finder: local module finder @11028347 (roots: /Users/rbabu/projects/softwares/jboss-eap-6.4/modules,
 /Users/rbabu/projects/softwares/jboss-eap-6.4/modules/system/layers/base))]`
 
 	at org.jboss.modules.ModuleClassLoader.findClass(ModuleClassLoader.java:213) [jboss-modules.jar:1.3.6.Final-redhat-1]
 	at org.jboss.modules.ConcurrentClassLoader.performLoadClassUnchecked(ConcurrentClassLoader.java:459) [jboss-modules.jar:1.3.6.Final-redhat-1]
 	at org.jboss.modules.ConcurrentClassLoader.performLoadClassChecked(ConcurrentClassLoader.java:408) [jboss-modules.jar:1.3.6.Final-redhat-1]
 	at org.jboss.modules.ConcurrentClassLoader.performLoadClass(ConcurrentClassLoader.java:389) [jboss-modules.jar:1.3.6.Final-redhat-1]
 	at org.jboss.modules.ConcurrentClassLoader.loadClass(ConcurrentClassLoader.java:134) [jboss-modules.jar:1.3.6.Final-redhat-1]
 	at java.lang.Class.forName0(Native Method) [rt.jar:1.8.0_131]
 	at java.lang.Class.forName(Class.java:264) [rt.jar:1.8.0_131]
 	at org.apache.ws.security.util.Loader.loadClass2(Loader.java:271) [wss4j-1.6.17.SP1-redhat-1.jar:1.6.17.SP1-redhat-1]
 	at org.apache.ws.security.util.Loader.loadClass(Loader.java:265) [wss4j-1.6.17.SP1-redhat-1.jar:1.6.17.SP1-redhat-1]
 	at org.apache.ws.security.util.Loader.loadClass(Loader.java:245) [wss4j-1.6.17.SP1-redhat-1.jar:1.6.17.SP1-redhat-1]
 	at org.apache.ws.security.WSSConfig.addJceProvider(WSSConfig.java:868) [wss4j-1.6.17.SP1-redhat-1.jar:1.6.17.SP1-redhat-1]
 	at org.apache.ws.security.WSSConfig$5.run(WSSConfig.java:446) [wss4j-1.6.17.SP1-redhat-1.jar:1.6.17.SP1-redhat-1]
 	at org.apache.ws.security.WSSConfig$5.run(WSSConfig.java:443) [wss4j-1.6.17.SP1-redhat-1.jar:1.6.17.SP1-redhat-1]
 	at java.security.AccessController.doPrivileged(Native Method) [rt.jar:1.8.0_131]
 	at org.apache.ws.security.WSSConfig.init(WSSConfig.java:443) [wss4j-1.6.17.SP1-redhat-1.jar:1.6.17.SP1-redhat-1]
 	at org.jboss.wsf.stack.cxf.config.CXFStackConfig.<init>(CXFStackConfigFactory.java:61) [jbossws-cxf-server-4.3.4.Final-redhat-1.jar:4.3.4.Final-redhat-1]
 	at org.jboss.wsf.stack.cxf.config.CXFStackConfigFactory.getStackConfig(CXFStackConfigFactory.java:45) [jbossws-cxf-server-4.3.4.Final-redhat-1.jar:4.3.4.Final-redhat-1]
 	at org.jboss.ws.common.management.AbstractServerConfig.create(AbstractServerConfig.java:272) [jbossws-common-2.3.1.Final-redhat-1.jar:2.3.1.Final-redhat-1]
 	at org.jboss.as.webservices.config.ServerConfigImpl.create(ServerConfigImpl.java:62) [jboss-as-webservices-server-integration-7.5.0.Final-redhat-21.jar:7.5.0.Final-redhat-21]
 	at org.jboss.as.webservices.service.ServerConfigService.start(ServerConfigService.java:72) [jboss-as-webservices-server-integration-7.5.0.Final-redhat-21.jar:7.5.0.Final-redhat-21]
 	at org.jboss.msc.service.ServiceControllerImpl$StartTask.startService(ServiceControllerImpl.java:1980)
 	at org.jboss.msc.service.ServiceControllerImpl$StartTask.run(ServiceControllerImpl.java:1913)
 	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1142) [rt.jar:1.8.0_131]
 	at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:617) [rt.jar:1.8.0_131]
 	at java.lang.Thread.run(Thread.java:748) [rt.jar:1.8.0_131]`
 
 https://developer.jboss.org/thread/175395
 https://www.bouncycastle.org/java.html
 https://github.com/RestComm/sip-servlets/pull/179/commits/94527442b66c01ad370d9c20666866218debbee2
 
* Add `<module name="org.bouncycastle" />` to `org/jboss/as/webservices/main/module.xml`
* Add `<module name="org.bouncycastle" />` to `org/jboss/as/webservices/server/integration/main/module.xml`
 
 
## References

 + https://access.redhat.com/solutions/209263    
 + https://developer.jboss.org/wiki/GenericHeaderBasedAuthentication
 + https://developer.jboss.org/wiki/WebAuthenticationUsingHTTPRequestParameters
 + https://developer.jboss.org/wiki/AS7EAP6CustomAuthenticatorValves-WritingAndConfiguring
 + https://access.redhat.com/documentation/en-us/jboss_enterprise_application_platform/6.1/html/security_guide/use_a_third-party_authentication_system_in_your_application
 + https://access.redhat.com/documentation/en-us/red_hat_jboss_enterprise_application_platform/6.4/single/security_architecture/
 + https://access.redhat.com/documentation/en-US/JBoss_Enterprise_Application_Platform/6.4/html/Security_Guide/Use_A_Third-Party_Authentication_System_In_Your_Application.html