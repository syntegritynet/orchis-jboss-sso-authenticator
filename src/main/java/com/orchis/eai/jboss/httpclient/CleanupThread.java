package com.orchis.eai.jboss.httpclient;

import org.apache.http.impl.client.CloseableHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

public class CleanupThread extends Thread {
    private static final Logger log = LoggerFactory.getLogger(CleanupThread.class);
    private CloseableHttpClient client;
    private long reapInterval;
    private long idleTimeout;

    public CleanupThread(CloseableHttpClient  client, long reapInterval, long idleTimeout) {
        this.client = client;
        this.reapInterval = reapInterval;
        this.idleTimeout = idleTimeout;
    }

    @Override
    public void run() {
        while (true) {
            log.debug("Reaping stale connections");
            client.getConnectionManager().closeExpiredConnections();
            client.getConnectionManager().closeIdleConnections(idleTimeout, TimeUnit.SECONDS);
            try {
                Thread.sleep(reapInterval);
            } catch (InterruptedException e) {
                /* We got interrupted so close it out */
                return;
            }
        }
    }
}
