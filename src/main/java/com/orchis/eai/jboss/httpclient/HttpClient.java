package com.orchis.eai.jboss.httpclient;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Map;
import javax.net.ssl.SSLContext;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.AllowAllHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HttpClient {

  private static final Logger log = LoggerFactory.getLogger(HttpClient.class);

  private final CloseableHttpClient httpClient;

  public CloseableHttpClient getInstance() {
    return httpClient;
  }

  public HttpClient(Map<String, String> clientProperties) {
    log.debug("initializing http client from map {}" , clientProperties);
    this.httpClient = buildHttpClient(Integer.valueOf(clientProperties.getOrDefault("connectTimeoutInMillis", "10000")),
        Integer.valueOf(clientProperties.getOrDefault("readTimeoutInMillis", "10000")),
        Integer.valueOf(clientProperties.getOrDefault("maxTotal", "100")),
        Integer.valueOf(clientProperties.getOrDefault("maxPerRoute", "10")),
        Boolean.valueOf(clientProperties.getOrDefault("trustAllCertificates", "false")),
        Boolean.valueOf(clientProperties.getOrDefault("trustSelfSignedCertificates", "true")),
        Boolean.valueOf(clientProperties.getOrDefault("ignoreNotMatchingHostnameInCertificate", "true")));

    (new CleanupThread(this.httpClient,
            Long.valueOf(clientProperties.getOrDefault("reapInterval", "300000")),
            Long.valueOf(clientProperties.getOrDefault("idleTimeout", "60")))).start();
  }

  private CloseableHttpClient buildHttpClient(int connectTimeoutInMillis, int readTimeoutInMillis, int maxTotal,
      int maxPerRoute, boolean trustAllCertificates, boolean trustSelfSignedCertificates,
      boolean ignoreNotMatchingHostnameInCertificate) {
    log.debug("initializing http client instance {} {} {} {} {} {} ", connectTimeoutInMillis,
        readTimeoutInMillis, maxTotal, maxPerRoute, trustAllCertificates, trustSelfSignedCertificates,
        ignoreNotMatchingHostnameInCertificate);

    RequestConfig requestConfig = RequestConfig.custom()
        .setConnectionRequestTimeout(connectTimeoutInMillis)
        .setConnectTimeout(connectTimeoutInMillis).setSocketTimeout(readTimeoutInMillis).build();

    PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager(
        buildSocketFactoryRegistry(trustAllCertificates, trustSelfSignedCertificates,
            ignoreNotMatchingHostnameInCertificate));
    cm.setMaxTotal(maxTotal);
    cm.setDefaultMaxPerRoute(maxPerRoute);
    cm.closeExpiredConnections();

    return HttpClients.custom().setDefaultRequestConfig(requestConfig).
        setConnectionManager(cm).build();
  }


  private static final X509HostnameVerifier anyHostnameVerifier = new AllowAllHostnameVerifier();

  private static final TrustStrategy trustAllCertificatesStrategy = new TrustStrategy() {
    @Override
    public boolean isTrusted(X509Certificate[] x509Certificates, String s)
        throws CertificateException {
      return true;
    }
  };

  private Registry<ConnectionSocketFactory> buildSocketFactoryRegistry(boolean trustAllCertificates,
      boolean trustSelfSignedCertificates, boolean ignoreNotMatchingHostnameInCertificate) {

    log.debug("initializing socket registry with http and https schemes");
    boolean overrideTrustCertificatePolicy = trustAllCertificates || trustSelfSignedCertificates ||
        ignoreNotMatchingHostnameInCertificate;
    log.debug("overrideTrustCertificatePolicy {}", overrideTrustCertificatePolicy);
    if (!overrideTrustCertificatePolicy) {
      return registry(SSLConnectionSocketFactory.getSocketFactory());
    }
    X509HostnameVerifier hostnameVerifier =
        ignoreNotMatchingHostnameInCertificate ? anyHostnameVerifier :
            SSLConnectionSocketFactory.BROWSER_COMPATIBLE_HOSTNAME_VERIFIER;
    SSLContext sslContext = null;
    try {
      TrustStrategy trustStrategy = null;
      if(trustAllCertificates) {
        trustStrategy = trustAllCertificatesStrategy;
        log.debug("ssl built with custom trust all strategy");
      } else if (trustSelfSignedCertificates) {
        trustStrategy = new TrustSelfSignedStrategy();
        log.debug("ssl built with custom self signed strategy");
      }

      if(trustStrategy == null) {
        sslContext = SSLContext.getDefault();
        log.debug("ssl built with default strategy");
      } else {
        SSLContextBuilder builder = SSLContexts.custom();
        builder.loadTrustMaterial(null, trustStrategy);
        sslContext = builder.build();
      }

    } catch (Exception e) {
      e.printStackTrace();
      log.error("Could not build SSL factory {} ", e.getMessage());
    }
    return registry(new SSLConnectionSocketFactory(sslContext, hostnameVerifier));
  }

  private Registry<ConnectionSocketFactory> registry(ConnectionSocketFactory sslSocketFactory) {
    return RegistryBuilder.<ConnectionSocketFactory>create()
        .register("http", PlainConnectionSocketFactory.INSTANCE)
        .register("https", sslSocketFactory)
        .build();
  }
}
