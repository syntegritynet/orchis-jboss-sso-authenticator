package com.orchis.eai.jboss.jaas.authenticator;

import com.orchis.eai.jboss.groups.UserGroups;
import com.orchis.eai.jboss.groups.UserGroupsExternalIdentityImpl;
import com.orchis.eai.jboss.roles.impl.RolePropertyParser;
import com.orchis.eai.jboss.roles.impl.UserRoles;
import com.orchis.eai.jboss.roles.impl.UserRolesFromFileImpl;
import java.io.IOException;
import java.io.InputStream;
import java.security.acl.Group;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;
import javax.security.auth.login.LoginException;
import org.jboss.security.SimpleGroup;
import org.jboss.security.SimplePrincipal;
import org.jboss.security.auth.spi.UsernamePasswordLoginModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OrchISLoginModule extends UsernamePasswordLoginModule {

  private static final Logger log = LoggerFactory.getLogger(OrchISLoginModule.class);

  private static final boolean fetchGroupsFromSession;
  private static final String applicationName;
  private static Map<String, List<String>> roleToGroupMappings;
  private static UserGroups userGroupsImpl;
  private static UserRoles rolesImpl;
  private String sessionIdentifier;

  static {
    Properties apiProviderProperties = new Properties();
    Map<String, String> apiProviderPropertiesMap;
    try {
      InputStream propsStream = OrchISLoginModule.class.getClassLoader().getResourceAsStream("orchis-connection.properties");
      if( null != propsStream) {
        log.debug("loading orchis-connection.properties");
        apiProviderProperties.load(propsStream);
        apiProviderPropertiesMap = apiProviderProperties.entrySet().stream()
            .collect(Collectors.toMap(e -> e.getKey().toString(), e -> e.getValue().toString()));

        fetchGroupsFromSession = Boolean.parseBoolean(apiProviderPropertiesMap
            .getOrDefault("fetchGroupsFromSession", "true"));
        applicationName = apiProviderPropertiesMap
            .getOrDefault("applicationName", "default");
      } else {
        throw new RuntimeException("Cannot continue loading module due to non existent orchis-connection file");
      }

    } catch (IOException e) {
      log.error("Error loading values from orchis-connection.properties" + e.getMessage());
      throw new RuntimeException("Cannot continue loading module");
    }

    Properties roleFileMappingProperties = new Properties();
    String defaultRoleName;
    try {
      InputStream roleFileStream = OrchISLoginModule.class.getClassLoader()
          .getResourceAsStream("role-mapping.properties");
      if( null != roleFileStream) {
        roleFileMappingProperties.load(roleFileStream);
        String rolesToGroupFilePath = roleFileMappingProperties.getProperty("roleFilePath");
        defaultRoleName = roleFileMappingProperties.getProperty("defaultRole");
        log.debug("role-mapping.properties: {} {} ", rolesToGroupFilePath, defaultRoleName);
        if (null != rolesToGroupFilePath) {
          roleToGroupMappings = new RolePropertyParser(rolesToGroupFilePath).getRoleToGroupMappings();
        }
        roleFileStream.close();
      } else {
        throw new RuntimeException("Cannot continue loading module due to non existent role-mapping file");
      }
    } catch (IOException e) {
      log.error("Error loading values from role-mapping.properties" + e.getMessage());
      throw new RuntimeException("Cannot continue loading module");
    }

    userGroupsImpl = new UserGroupsExternalIdentityImpl(apiProviderPropertiesMap);
    rolesImpl = new UserRolesFromFileImpl(roleToGroupMappings, defaultRoleName);
  }

  @Override
  protected Group[] getRoleSets() throws LoginException {
    Group[] groups = {new SimpleGroup("Roles")};
    log.debug("Finding groups for user {} ", super.getUsername());
    Set<String> userGroups = null;
    if (fetchGroupsFromSession) {
      userGroups = userGroupsImpl.getUserGroups(this.getUsersPassword());
      log.debug("Fetched groups for user {} ", userGroups);
    } else {
      log.error("Not supported configuration ! fetchGroupsFromSession must be set to true");
    }

    Set<String> allUserRoles = rolesImpl.getUserRoles(applicationName, userGroups);
    log.debug("application roles for user : {}", allUserRoles);

    //Set application roles for user, if it was returned from the group role mapping
    if (null != allUserRoles && !allUserRoles.isEmpty()) {
      for (String applicableRole : allUserRoles) {
        SimplePrincipal role = new SimplePrincipal(applicableRole);
        groups[0].addMember(role);
        log.debug("Set role: {}", applicableRole);
      }
    }
    return groups;
  }

  /**
   * A hook that allows subclasses to change the validation of the
   * input password against the expected password. This version
   * checks that neither inputPassword or expectedPassword are null
   * and that inputPassword.equals(expectedPassword) is true;
   *
   * @return true if the inputPassword is valid, false otherwise.
   */
  protected boolean validatePassword(String inputPassword,
      String expectedPassword) {
    //We would not do session validation; if required it could be added later
    // but if it is reaching till this point, that means user has reached
    // here allowed by gateway and when user tries to fetch the groups
    //there would be no response

    log.debug("Inside validate password of login module");
    //Set session key to fetch groups of incoming user from underlying identity system
    sessionIdentifier = inputPassword;
    return true;
  }

  @Override
  protected String getUsersPassword() throws LoginException {
    return sessionIdentifier;
  }
}