package com.orchis.eai.jboss.roles.impl;

import java.util.Set;

public interface UserRoles {

  Set<String> getUserRoles(String applicationName, Set<String> groupNames);

  Set<String> getUserRolesUsingUserId(String applicationName, String userId);

  Set<String> getUserRolesUsingSessionToken(String applicationName, String token);
}
