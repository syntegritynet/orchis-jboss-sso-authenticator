package com.orchis.eai.jboss.roles.impl;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.tomcat.util.json.JSONArray;
import org.apache.tomcat.util.json.JSONObject;
import org.apache.tomcat.util.json.JSONTokener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RolePropertyParser {

  private static final Logger logger = LoggerFactory.getLogger(RolePropertyParser.class);

  private final String filePath;

  public RolePropertyParser(String filePath) {
    this.filePath = filePath;
  }

  public Map<String, List<String>> getRoleToGroupMappings() {

    logger.debug("Inside getRoleToGroupMappings");
    //Parse the file to appropriate structure
    InputStream is = getFileInputStream();
    //Feed in to get the JSONObject
    JSONObject jsonRoleGroupMapping = getJsonfieldMapping(is);
    //extract the details from JSONObject
    Map<String, List<String>> roleToGroupMappings = getRoleToGroupMappings(jsonRoleGroupMapping);
    logger.debug("roleToGroupMappings -->{} ", roleToGroupMappings);
    return roleToGroupMappings;
  }

  private InputStream getFileInputStream() {
    try {
      return new FileInputStream(filePath);
    } catch (Exception e) {
      logger.error("!!! Unable to parse the input file for role mapping {}", e.getMessage());
    }
    return null;
  }

  private JSONObject getJsonfieldMapping(InputStream is) {
    try {
      return new JSONObject(new JSONTokener(new InputStreamReader(is)));
    } catch (Exception e) {
      logger.error("!!! Exception occurred during json field mapping {}", e.getMessage());
    }
    return null;
  }

  private Map<String, List<String>> getRoleToGroupMappings(JSONObject jsonRoleGroupMapping) {
    logger.debug("getRoleToGroupMappings -->{} ", jsonRoleGroupMapping);
    try {
      Iterator keys = jsonRoleGroupMapping.keys();
      Map<String, List<String>> mappings = new HashMap<>();
      while (keys.hasNext()) {
        String mappingKey = (String) keys.next();
        JSONArray groups = (JSONArray) jsonRoleGroupMapping.get(mappingKey);
        List<String> listOfGroups = new ArrayList<>();
        for (int i = 0; i < groups.length(); i++) {
          listOfGroups.add(groups.getString(i));
        }
        logger.debug("mappings --> {}, {} ", mappings, listOfGroups);
        mappings.put(mappingKey, listOfGroups);
      }
      return mappings;
    } catch (Exception e) {
      logger.error("!!! Exception occurred during getRoleToGroupMappings {}", e.getMessage());
    }
    return null;
  }
}