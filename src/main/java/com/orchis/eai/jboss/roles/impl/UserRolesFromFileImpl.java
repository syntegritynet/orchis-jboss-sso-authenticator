package com.orchis.eai.jboss.roles.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserRolesFromFileImpl implements UserRoles {

  private static final Logger log = LoggerFactory.getLogger(UserRolesFromFileImpl.class);
  private final Map<String, List<String>> roleToGroupMappings;
  private final String defaultRoleName;

  public UserRolesFromFileImpl(Map<String, List<String>> roleToGroupMappings,
      String defaultRoleName) {
    this.roleToGroupMappings = roleToGroupMappings;
    this.defaultRoleName = defaultRoleName;
  }

  public Set<String> getUserRoles(String applicationName, Set<String> groupNames) {

    log.debug("Get all user roles based on groupNames");
    //1. Get all roles of user ; this must be a one time static read for the container
    Set<String> allUserRoles = new HashSet<>();
    if( null != roleToGroupMappings && !roleToGroupMappings.isEmpty()) {
      Set<String> allRolesForApplication = roleToGroupMappings.keySet();
      log.debug("allRolesForApplication --> {}", allRolesForApplication);
      if (null != groupNames && !groupNames.isEmpty()) {
        allUserRoles = getAllUserRoles(allRolesForApplication, groupNames);
        log.debug("allUserRoles before default --> {}", allUserRoles);
      }
      log.debug("allUserRoles --> {}", allUserRoles);
    }
    if( null != defaultRoleName) allUserRoles.add(defaultRoleName);
    return allUserRoles;
  }

  private Set<String> getAllUserRoles(Set<String> allRolesForApplication, Set<String> groupNames) {
    Set<String> allRoles = new HashSet<>();
    for (String roleName : allRolesForApplication) {
      if (isUserInRole(roleName, groupNames)) {
        allRoles.add(roleName);
      }
    }
    log.debug("In getAllUserRoles --> {}", allRoles);
    return allRoles;
  }

  private boolean isUserInRole(String roleName, Set<String> groupNames) {
    log.debug("In isUserInRole {} {}", roleName, groupNames);
    List<String> allowedGroupsForRole = roleToGroupMappings.get(roleName);
    log.debug("In isUserInRole allowedGroupsForRole {}", allowedGroupsForRole);
    //use case for find if there is duplicate in 2 lists
    for (String allowedGroup : allowedGroupsForRole) {
      if (groupNames.contains(allowedGroup)) {
        return true;
      }
    }
    log.debug("Cannot find user role ");
    return false;
  }

  public Set<String> getUserRolesUsingUserId(String applicationName, String userId) {
    return null;
  }

  public Set<String> getUserRolesUsingSessionToken(String applicationName, String token) {
    return null;
  }
}