package com.orchis.eai.jboss.groups;

import java.util.Set;
import javax.security.auth.login.LoginException;

public interface UserGroups {

    Set<String> getUserGroups(String userlookupIdentifier) throws LoginException;
}
