package com.orchis.eai.jboss.groups;

import com.orchis.eai.jboss.httpclient.HttpClient;
import java.nio.charset.Charset;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.security.auth.login.LoginException;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.tomcat.util.json.JSONArray;
import org.apache.tomcat.util.json.JSONObject;
import org.apache.tomcat.util.json.JSONTokener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserGroupsExternalIdentityImpl implements UserGroups {
  private static final Logger log = LoggerFactory.getLogger(UserGroupsExternalIdentityImpl.class);
  private final String endpoint;
  private final String cookieName;
  private final CloseableHttpClient httpClient;

  public UserGroupsExternalIdentityImpl(Map<String, String> providerPropertiesMap) {
    this.endpoint = providerPropertiesMap.getOrDefault("url", "http://localhost:7080");
    this.cookieName = providerPropertiesMap.getOrDefault("cookieName", "token");
    httpClient = new HttpClient(providerPropertiesMap).getInstance();
    log.debug("providerPropertiesMap {}", providerPropertiesMap);
  }

  public Set<String> getUserGroups(String session) throws LoginException{
    log.debug("Inside external identity impl to fetch groups for session key: {} ", session);
    Set<String> userGroupsToBeReturned = new HashSet<>();
    try {
      HttpGet request = new HttpGet(endpoint);
      request.setHeader(cookieName, session);
      HttpResponse resp = httpClient.execute(request);

      if (resp.getStatusLine().getStatusCode() == 200) {
        String responseBody = EntityUtils.toString(resp.getEntity(), Charset.defaultCharset());
        log.debug("responseBody {}", responseBody);
        try {
          JSONObject obj = new JSONObject(new JSONTokener(responseBody));
          Object grpObject = obj.opt("groups");
          log.debug("groups parsed from response {}" , grpObject);
          if (null != grpObject && !JSONObject.NULL.equals(grpObject)) {
            JSONArray groupArray = (JSONArray) grpObject;
            log.debug("OrchIS session groups: {} ", groupArray);
            for (int i = 0; i < groupArray.length(); i++) {
              userGroupsToBeReturned.add(groupArray.getString(i));
            }
          } else {
            log.warn("user does not belong to any group!");
          }
          log.debug("successfully returning from groups impl with {}", userGroupsToBeReturned);
          return userGroupsToBeReturned;
        } catch (RuntimeException re) {
          log.error("Unable to parse orchis Session API : {}", re.getMessage());
        }
      } else {
        log.error("Unexpected response code from OrchIS :{}", resp.getStatusLine().getStatusCode());
      }
    } catch (Exception e) {
      log.error("Error while fetching group membership using the session token : {}", e.getMessage());
    }
    log.error("Unable to identity user with session to retrieve group membership");
    throw new LoginException("Cannot identify logged in user using session token");
  }
}