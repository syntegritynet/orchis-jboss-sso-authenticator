package com.orchis.eai.jboss.jaas.authenticator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockserver.integration.ClientAndServer.startClientAndServer;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;

import java.security.acl.Group;
import java.util.ArrayList;
import java.util.List;
import javax.security.auth.login.LoginException;
import org.jboss.security.SimplePrincipal;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockserver.integration.ClientAndServer;

public class OrchISLoginModuleTest {

  private ClientAndServer server;

  @Before
  public void setup() {

    server = startClientAndServer(4002);
  }

  @After
  public void tearDownServer() {
    server.stop();
  }

  @Test
  public void testGetRoleSets() throws Exception {

    server.when(request().withMethod("GET").withPath("/overlay/api/session")).respond(response().withStatusCode(200).withBody("{\"groups\": [ \"adminease\", \"ebesa\"]}"));

    List<String> expectedRoles = new ArrayList<>();
    expectedRoles.add("admin");
    expectedRoles.add("developer");
    expectedRoles.add("default");

    OrchISLoginModule module = new OrchISLoginModule();
    try {
      assertRoles(expectedRoles, module.getRoleSets()[0]);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Test
  public void testGetRoleSetsWithEmptyGroups() throws Exception {

    server.when(request().withMethod("GET").withPath("/overlay/api/session")).respond(response().withStatusCode(200).withBody("{\"groups\": []}"));

    OrchISLoginModule module = new OrchISLoginModule();
    List<String> expectedRoles = new ArrayList<>();
    expectedRoles.add("default");
    assertRoles(expectedRoles, module.getRoleSets()[0]);

  }

  @Test
  public void testGetRoleSetsWithNoGroups() throws Exception {

    server.when(request().withMethod("GET").withPath("/overlay/api/session")).respond(response().withStatusCode(200).withBody("{\"name\":\"asdf\"}"));

    OrchISLoginModule module = new OrchISLoginModule();
    List<String> expectedRoles = new ArrayList<>();
    expectedRoles.add("default");
    assertRoles(expectedRoles, module.getRoleSets()[0]);
  }

  @Test
  public void testGetRoleSetsWithInvalidSession() throws Exception {

    server.when(request().withMethod("GET").withPath("/overlay/api/session")).respond(response().withStatusCode(401).withBody("{}"));

    OrchISLoginModule module = new OrchISLoginModule();
    try {
      module.getRoleSets();
      fail("must throw LoginException");
    } catch (LoginException ex) {
      assertEquals("Cannot identify logged in user using session token", ex.getMessage());
    }
  }

  @Test
  public void testGetRoleSetsWhenSessionServiceThrows500() throws Exception {

    server.when(request().withMethod("GET").withPath("/overlay/api/session")).respond(response().withStatusCode(500).withBody("{}"));

    OrchISLoginModule module = new OrchISLoginModule();
    try {
      module.getRoleSets();
      fail("must throw LoginException");
    } catch (LoginException ex) {
      assertEquals("Cannot identify logged in user using session token", ex.getMessage());
    }
  }


  private void assertRoles(List<String> expectedRoles, Group actualRoles) {
      for(String expectedRole: expectedRoles) {
        assertTrue("Member has expected role of " + expectedRole, actualRoles.isMember(new SimplePrincipal(expectedRole)));
      }
  }

}
