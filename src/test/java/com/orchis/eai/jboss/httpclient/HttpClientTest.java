package com.orchis.eai.jboss.httpclient;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockserver.integration.ClientAndServer.startClientAndServer;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.HttpsURLConnection;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.After;
import org.junit.Before;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.socket.SSLFactory;


public class HttpClientTest {

  private ClientAndServer server;

  @Before
  public void setup() {
    server = startClientAndServer(4002);
  }

  @After
  public void tearDownServer() {
    server.stop();
  }

  @Test
  public void testHttpEndPoint() {

    server.when(request().withMethod("GET").withPath("/whatever")).respond(response().withStatusCode(200).withBody(""));
    Map<String, String> connProperties = new HashMap<>();
    connProperties.put("connectTimeoutInMillis", "100");
    connProperties.put("readTimeoutInMillis", "100");
    connProperties.put("maxTotal", "100");
    connProperties.put("maxPerRoute", "100");
    connProperties.put("trustAllCertificates", "true");
    connProperties.put("trustSelfSignedCertificates", "true");
    connProperties.put("ignoreNotMatchingHostnameInCertificate", "true");

    HttpClient hc = new HttpClient(connProperties);
    try {
      CloseableHttpResponse response = hc.getInstance().execute(new HttpGet("http://localhost:4002/whatever"));
      assertEquals(200, response.getStatusLine().getStatusCode());
    } catch (IOException e) {
      e.printStackTrace();
    }

  }

  @Test
  public void testHttpEndPointWithConnectTimeout() {

    server.when(request().withMethod("GET").withPath("/whatever")).
        respond(response().withDelay(TimeUnit.MILLISECONDS, 110).withStatusCode(200).withBody(""));
    Map<String, String> connProperties = new HashMap<>();
    connProperties.put("connectTimeoutInMillis", "100");
    connProperties.put("readTimeoutInMillis", "100");
    connProperties.put("maxTotal", "100");
    connProperties.put("maxPerRoute", "100");
    connProperties.put("trustAllCertificates", "true");
    connProperties.put("trustSelfSignedCertificates", "true");
    connProperties.put("ignoreNotMatchingHostnameInCertificate", "true");

    HttpClient hc = new HttpClient(connProperties);
    try {
      hc.getInstance().execute(new HttpGet("http://localhost:4002/whatever"));
      fail("should throw socketTimeoutException");
    } catch (SocketTimeoutException e) {
      e.printStackTrace();
      assertTrue(true);
    } catch (IOException e) {
      e.printStackTrace();
      fail("should throw socketTimeoutException and not IOException");
    }

  }

  @Test
  public void testHttpEndPointWithSslAndTrustAll() {

    server.when(request().withMethod("GET").withPath("/whatever"))
          .respond(response().withBody(""));

    Map<String, String> connProperties = new HashMap<>();
    connProperties.put("connectTimeoutInMillis", "10000");
    connProperties.put("readTimeoutInMillis", "10000");
    connProperties.put("maxTotal", "100");
    connProperties.put("maxPerRoute", "100");
    connProperties.put("trustAllCertificates", "true");
    connProperties.put("trustSelfSignedCertificates", "true");
    connProperties.put("ignoreNotMatchingHostnameInCertificate", "true");

    HttpClient hc = new HttpClient(connProperties);
    try {
      CloseableHttpResponse response = hc.getInstance().execute(new HttpGet("https://localhost:4002/whatever"));
      assertEquals(200, response.getStatusLine().getStatusCode());
    } catch (Exception e) {
      e.printStackTrace();
      fail("should not throw EXception");
    }

  }


  @Test
  @Ignore
  public void testHttpEndPointWithSslAndDontTrustConfig() {

    server.when(request().withMethod("GET").withPath("/whatever")).respond(response().withBody(""));

    Map<String, String> connProperties = new HashMap<>();
    connProperties.put("connectTimeoutInMillis", "1000");
    connProperties.put("readTimeoutInMillis", "100");
    connProperties.put("maxTotal", "100");
    connProperties.put("maxPerRoute", "100");
    connProperties.put("trustAllCertificates", "false");
    connProperties.put("trustSelfSignedCertificates", "false");
    connProperties.put("ignoreNotMatchingHostnameInCertificate", "false");

    HttpClient hc = new HttpClient(connProperties);
    try {
      hc.getInstance().execute(new HttpGet("https://localhost:4002/whatever"));
      fail("Should not succeed");
    } catch (Exception e) {
      if( e instanceof javax.net.ssl.SSLException) {
        assertTrue("Expected SSL Exception", true);
      }
    }
  }


  @Test
  @Ignore
  public void testHttpEndPointWithSslWithTrustedContext() {

    // Make sure that all connection using HTTPS will use the SSL Context defined by MockServer, allowing auto-generated self-signed certificate of the Proxy to be accepted
    // https://github.com/jamesdbloom/mockserver/issues/205
    HttpsURLConnection.setDefaultSSLSocketFactory(SSLFactory.getInstance().sslContext().getSocketFactory());

    server.when(request().withMethod("GET").withPath("/whatever")).respond(response().withBody(""));
    Map<String, String> connProperties = new HashMap<>();
    connProperties.put("connectTimeoutInMillis", "1000");
    connProperties.put("readTimeoutInMillis", "100");
    connProperties.put("maxTotal", "100");
    connProperties.put("maxPerRoute", "100");
    connProperties.put("trustAllCertificates", "false");
    connProperties.put("trustSelfSignedCertificates", "false");
    connProperties.put("ignoreNotMatchingHostnameInCertificate", "false");

    HttpClient hc = new HttpClient(connProperties);
    try {
      CloseableHttpResponse response = hc.getInstance().execute(new HttpGet("https://localhost:4002/whatever"));
      assertEquals(200, response.getStatusLine().getStatusCode());
    } catch (SocketTimeoutException e) {
      e.printStackTrace();
      fail("should not throw socketTimeoutException");
    } catch (IOException e) {
      e.printStackTrace();
      fail("should not throw IOEXception");
    }
  }

}
