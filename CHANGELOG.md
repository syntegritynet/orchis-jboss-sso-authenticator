## [Unreleased]
### Changed
- Add a cleanup thread to reap expired and extra idle connections

## [1.1] - 2017-11-15
### Changed
- Fixed HttpClient to behave in singleton mode
- throw login exception if session response to retrieve group is invalid
- throw runtime exception if property files are not available at load time
- maven dependency to provided for artifacts already in jboss eap 6.4 modules

### Added
- Added more test cases for login module and httpclient
- Added httpclient configurable params to support http(s) endpoints

### Removed
- Removed maven shade plugin 

## [1.0]
- Release with OrchIS group service and session service integration
- read and parse group to role from property file
